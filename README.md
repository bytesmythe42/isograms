# isograms

Lists of isograms based on the wordlist from https://github.com/dwyl/english-words

isograms.txt is the base list of isograms

isopairs.zip contains isopairs.txt

isopairs.txt is the list of all pairs of isograms that are themselves isograms. Each line is preceded by the total letter count of both words.
